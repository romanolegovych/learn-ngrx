import 'reflect-metadata';
import 'zone.js';
import './styles/styles.scss';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';

if (module.hot) {
    module.hot.accept();
}

// Note: @ng-tools/webpack looks for the following expression when performing productions
// builds. Don't change how this line looks, otherwise you may break tree-shaking.
const modulePromise = platformBrowserDynamic().bootstrapModule(AppModule);
