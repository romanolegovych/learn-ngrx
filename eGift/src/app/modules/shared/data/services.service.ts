import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export interface IServices {
    id?: number;
    name: string;
    price: number;
    employeeId?: number;
}

export interface IServicesService {
    getServiceList(): Observable<IServices[]>;
    getService(id: number): Observable<IServices>;
    updateService(service: IServices): Observable<IServices>;
    creteService(employee: IServices): Observable<IServices>;
    deleteService(id: number): Observable<number>;
}

@Injectable()
export class ServicesService implements IServicesService {
    // todo: refactor into base class
    constructor(
        private http: HttpClient
    ) { }

    getServiceList(): Observable<IServices[]> {
        return this.http.get<IServices[]>('api/services');
    }

    getService(id: number): Observable<IServices> {
        return this.http.get<IServices>(`api/services/${id}`);
    }

    updateService(service: IServices): Observable<IServices> {
        return this.http.put<IServices>(`api/services/${service.id}`, service);
    }

    creteService(service: IServices): Observable<IServices> {
        return this.http.post<IServices>(`api/services`, service);
    }

    deleteService(id: number): Observable<any> {
        return this.http.delete(`api/services/${id}`)
    }


}