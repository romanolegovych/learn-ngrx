﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export interface IEmployee {
    id?: number;
    name: string;
    dateOfBirth?: Date;
}

export interface IEmployeeService {
    getEmployeeList(): Observable<IEmployee[]>;
    getEmployee(id: number): Observable<IEmployee>;
    updateEmployee(employee: IEmployee): Observable<IEmployee>;
    creteEmployee(employee: IEmployee): Observable<IEmployee>;
    deleteEmployee(id: number): Observable<IEmployee>;
}

@Injectable()
export class EmployeeService implements IEmployeeService {
    // todo: refactor into base class
    constructor(
        private http: HttpClient
    ) {  }

    getEmployeeList(): Observable<IEmployee[]> {
        return this.http.get<IEmployee[]>('api/employees');
    }

    getEmployee(id: number): Observable<IEmployee> {
        return this.http.get<IEmployee>(`api/employees/${id}`);
    }

    updateEmployee(employee: IEmployee): Observable<IEmployee> {
        return this.http.put<IEmployee>(`api/employees/${employee.id}`, employee);
    }

    creteEmployee(employee: IEmployee): Observable<IEmployee> {
        return this.http.post<IEmployee>(`api/employees`, employee);
    }

    deleteEmployee(id: number): Observable<any> {
        return this.http.delete(`api/employees/${id}`)
    }
}
