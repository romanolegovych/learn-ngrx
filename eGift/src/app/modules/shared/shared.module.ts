import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppMaterialModule } from '../shared/material/app.material.module';

import { EmployeeService } from './data/employee.service';
import { ServicesService } from './data/services.service';

@NgModule({
    imports: [
        HttpClientModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        AppMaterialModule
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [EmployeeService, ServicesService]
        }
    }
}

