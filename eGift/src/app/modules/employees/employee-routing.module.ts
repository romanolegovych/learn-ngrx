﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './components/employee/employee.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: EmployeeListComponent
    },
    {
        path: 'employee/:id',
        component: EmployeeComponent
    },
    {
        path: 'new-employee',
        component: EmployeeComponent
    },
    {
        path: '',
        redirectTo: 'dashboard'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class EmployeeRoutingModule { }
