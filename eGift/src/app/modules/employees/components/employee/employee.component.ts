﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IEmployee, EmployeeService } from '../../../shared/data/employee.service';

@Component({
    selector: 'egift-employee',
    templateUrl: './employee.component.html'
})
export class EmployeeComponent implements OnInit {
    public employee: IEmployee = {
        name: ''
    };

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private employeeService: EmployeeService
    ) { }

    ngOnInit() {
        let id = this.route.snapshot.paramMap.get('id');

        if (id) {
            this.employeeService.getEmployee(Number(id)).subscribe(employee => {
                this.employee = employee;
            });
        }
    }

    onSubmit(): void {
        if (this.employee.id) {
            this.employeeService.updateEmployee(this.employee).subscribe(data => {
                this.router.navigate(['/employees/dashboard']);
            });
        } else {
            this.employeeService.creteEmployee(this.employee).subscribe(data => {
                this.router.navigate(['/employees/dashboard']);
            });
        }
    }
}
