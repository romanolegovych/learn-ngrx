﻿import { Component, OnInit } from '@angular/core';
import { IEmployee, EmployeeService } from '../../../shared/data/employee.service';


@Component({
    selector: 'egift-employee-list',
    templateUrl: './employee-list.component.html'
})
export class EmployeeListComponent {
    public employeeList: Array<IEmployee>;

    constructor(
        private employeeService: EmployeeService
    ) { }

    ngOnInit() {
        this.getEmployeeList();    
    }

    getEmployeeList(): void {
        this.employeeService.getEmployeeList().subscribe(employeeList => {
            this.employeeList = employeeList;
        });
    }

    deleteEmployee(id: number): void {
        this.employeeService.deleteEmployee(id).subscribe(() => {
            this.getEmployeeList();
        });
    }
}
