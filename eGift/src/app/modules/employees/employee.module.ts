import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { EmployeeRoutingModule } from './employee-routing.module';

import { EmployeeComponent } from './components/employee/employee.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';

@NgModule({
    declarations: [
        EmployeeComponent,
        EmployeeListComponent
    ],
    imports: [
        EmployeeRoutingModule,
        SharedModule
    ]
})
export class EmployeeModule { }
