import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IServices, ServicesService } from '../../../shared/data/services.service';
import { IEmployee, EmployeeService } from '../../../shared/data/employee.service';

@Component({
    selector: 'egift-service',
    templateUrl: './service.component.html'
})
export class ServiceComponent implements OnInit {
    public service: IServices = {
        name: '',
        price: null
    };
    public employeeList: Array<IEmployee>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private servicesService: ServicesService,
        private employeeService: EmployeeService
    ) { }

    ngOnInit() {
        let id = this.route.snapshot.paramMap.get('id');

        if (id) {
            this.servicesService.getService(Number(id)).subscribe(service => {
                this.service = service;
            });
        }

        this.getEmployeeList();
    }

    onSubmit(): void {
        if (this.service.id) {
            this.servicesService.updateService(this.service).subscribe(data => {
                this.router.navigate(['/services/dashboard']);
            });
        } else {
            this.servicesService.creteService(this.service).subscribe(data => {
                this.router.navigate(['/services/dashboard']);
            });
        }
    }

    getEmployeeList(): void {
        this.employeeService.getEmployeeList().subscribe(employeeList => {
            this.employeeList = employeeList;
        });
    }
}
