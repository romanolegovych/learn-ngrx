﻿import { Component, OnInit } from '@angular/core';
import { IServices, ServicesService } from '../../../shared/data/services.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Component({
    selector: 'egift-service-list',
    templateUrl: './service-list.component.html'
})
export class ServiceListComponent {
    public serviceList: Array<IServices>;

    constructor(
        private servicesService: ServicesService
    ) { }

    ngOnInit() {
        this.getServiceList();
    }

    getServiceList(): void {
        this.servicesService.getServiceList()
            .map(serviceList => {
                return serviceList.filter(service => service.price > 100)
            })
            .take(10)
            .subscribe(serviceList => this.serviceList = serviceList);
    }

    deleteService(id: number): void {
        this.servicesService.deleteService(id).subscribe(() => {
            this.getServiceList();
        });
    }
}
