import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { ServiceRoutingModule } from './service-routing.module';

import { ServiceComponent } from './components/service/service.component';
import { ServiceListComponent } from './components/service-list/service-list.component';


@NgModule({
    declarations: [
        ServiceComponent,
        ServiceListComponent
    ],
    imports: [
        ServiceRoutingModule,
        SharedModule
    ]
})
export class ServiceModule { }
