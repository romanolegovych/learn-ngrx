﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceComponent } from './components/service/service.component';
import { ServiceListComponent } from './components/service-list/service-list.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: ServiceListComponent
    },
    {
        path: 'list/:id',
        component: ServiceComponent
    },
    {
        path: 'new-service',
        component: ServiceComponent
    },
    {
        path: '',
        redirectTo: 'dashboard'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ServiceRoutingModule { }
