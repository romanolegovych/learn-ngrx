﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eGift.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace eGift.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Service> Services { get; set; }
    }
}
