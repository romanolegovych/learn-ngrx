﻿namespace eGift.Data.Model
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}
