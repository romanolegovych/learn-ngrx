﻿using System;
using System.Collections.Generic;

namespace eGift.Data.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public List<Service> Services { get; set; }
    }
}
